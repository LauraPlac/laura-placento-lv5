﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization.Json;
using System.Text;

namespace lv5
{
    //Z3.
    class DataConsolePrinter
    {
        public void PrintData(IDataSet dataset)
        {
            ReadOnlyCollection<List<string>> dataToPrint = dataset.GetData();
            if(dataToPrint == null)
            {
                throw new Exception("No access to data");
            }

            foreach  (var subDataToPrint in dataToPrint)
            {
                foreach (var subData in subDataToPrint)
                {
                    Console.Write(subData + " ");
                }
                Console.WriteLine();
            }
            
        }
    }
}
