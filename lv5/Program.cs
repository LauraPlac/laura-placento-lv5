﻿using System;

namespace lv5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Z1.
            Product product = new Product("kruh", 5, 2);
            Product product1 = new Product("mlijeko", 8, 4);
            Product product2 = new Product("sir", 3, 1);

            Box box = new Box("Paket 1");
            box.Add(product);
            box.Add(product1);
            Box box1 = new Box("Paket 2");
            box1.Add(product2);
            box.Add(box1);

            Console.WriteLine(box.Description());
            Console.WriteLine("Cijena paketa:" + box.Price);
            Console.WriteLine("\n");

            //Z2.
            ShippingService shippingService = new ShippingService(2);
            Console.WriteLine("Cijena posiljke:" + shippingService.CalculateShippingService(box));
            Console.WriteLine("\n");

            //Z3.

            User user = User.GenerateUser("Laura");
            User user1 = User.GenerateUser("La");
            User user2 = User.GenerateUser("L");

            VirtualProxyDataSet virtualProxyDataset = new VirtualProxyDataSet(@"C:\Users\Laura\source\repos\lv5\CSV.txt");
            

            ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user);
            ProtectionProxyDataset protectionProxyDataset1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset protectionProxyDataset2 = new ProtectionProxyDataset(user2);

            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();

            try
            {
                dataConsolePrinter.PrintData(protectionProxyDataset1);
                dataConsolePrinter.PrintData(virtualProxyDataset);
                Console.WriteLine("\n");
                
                dataConsolePrinter.PrintData(protectionProxyDataset);
                dataConsolePrinter.PrintData(protectionProxyDataset2);
                
                
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("\n");

            //Z4.
            MyTheme myTheme = new MyTheme();
            LightTheme lightTheme = new LightTheme();
            ReminderNote reminderNote = new ReminderNote("Laura", myTheme);
            ReminderNote reminderNote1 = new ReminderNote("Placento", lightTheme);
            reminderNote.Show();
            reminderNote1.Show();


            Console.WriteLine("\n");

            //Z6.
            BubbleSort bubbleSort = new BubbleSort();
        
            NumberSequence numberSequence = new NumberSequence(5);

            numberSequence.SetSortStrategy(bubbleSort);
            numberSequence.InsertAt(3, 7);
            numberSequence.InsertAt(4, -1);
            Console.WriteLine(numberSequence);
            numberSequence.Sort();
            Console.WriteLine(numberSequence);


        }


       
    }


   
}
