﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv5
{
    class MyTheme : ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.DarkYellow;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
        }
        public string GetHeader(int width)
        {
            return new string('*', width);
        }
        public string GetFooter(int width)
        {
            return new string('*', width);
        }
    }
}
