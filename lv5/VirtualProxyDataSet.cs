﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace lv5
{
    //Z3.
    class VirtualProxyDataSet : IDataSet
    {
        private string filePath;
        private DataSet dataset;
        public VirtualProxyDataSet(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new DataSet(filePath);
            }
            return dataset.GetData();
        }
    }
}
