﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace lv5
{
    //Z3.

    interface IDataSet
    {
        ReadOnlyCollection<List<string>> GetData();
    }

}
