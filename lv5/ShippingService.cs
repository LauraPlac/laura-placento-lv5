﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv5
{
    //Z2.
    class ShippingService
    {
        public double PricePerKg { get; set; }

        public ShippingService(double pricePerKg)
        {
            PricePerKg = pricePerKg; 
        }
        

        public double CalculateShippingService(IShipable packet)
        {
            return packet.Weight * PricePerKg + packet.Price;
        }

    }
}
